begin transaction;
ALTER TABLE Customers DROP COLUMN is_author;
ALTER TABLE Customers ADD COLUMN balance INTEGER CHECK(balance>0);
ALTER TABLE Books DROP COLUMN author_id;
ALTER TABLE Books ADD COLUMN author VARCHAR(100);
ALTER TABLE Books ALTER COLUMN rate DROP DEFAULT ;
ALTER TABLE Books ALTER COLUMN description DROP NOT NULL;
CREATE TABLE Genres
(
   id bigserial not null PRIMARY KEY,
   book_id INT NOT NULL,
   CONSTRAINT book_id_fk FOREIGN KEY (book_id) REFERENCES Books (id),
   genre varchar(100) NOT NULL
);
commit transaction;