with aircrafts as (select aircraft_code, model from aircrafts group by aircraft_code, model)

select
    a.aircraft_code,
    a.model,
    count(fare_conditions = 'Business' or NULL) * 100.0 / count(*) as business_cnt
from aircrafts a
left join seats s on a.aircraft_code = s.aircraft_code
group by a.aircraft_code, a.model
order by business_cnt desc
limit 5;
--увеличение скорости с 16.5 до 4 ms
--использовались CTE для оптимизации группировки
--work_mem не менял так как результат не меняется
