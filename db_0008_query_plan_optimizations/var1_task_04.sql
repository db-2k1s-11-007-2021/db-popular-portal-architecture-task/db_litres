-- 4. Для каждого рейса определить были ли рейсы со статусом Cancelled из текущего аэропорта отправления в 10 рейсах ранее
-- (отсортируйте по scheduled_departure и flight_id). {flight_id, flight_no, departure_airport, scheduled_departure, has_prev10_cancelled}
set work_mem = '4MB';
set enable_seqscan to off;
set enable_sort to off;
EXPLAIN (ANALYZE, BUFFERS )
select flight_id,
       flight_no,
       departure_airport,
       scheduled_departure,
       count(status = 'Cancelled' or null)
       over (partition by departure_airport order by scheduled_departure, flight_id rows between 10 preceding and 1 preceding) >
       0 as has_prev10_cancelled
from flights;
-- С work_mem все в порядке, временные файлы на диске не используются.
-- Ничего оптимизировать не надо. Время выполнения малое, условий при поиске не требуется.
