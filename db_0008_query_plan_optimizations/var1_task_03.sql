set work_mem = 130720;
EXPLAIN (ANALYZE)
with unique_passengers_per_date_and_aircraft as (
    select a.aircraft_code,
           a.model,
           f.scheduled_departure::date    as scheduled_departure_date,
           count(distinct t.passenger_id) as unique_passenger_id_cnt
    from aircrafts a
             left join flights f on a.aircraft_code = f.aircraft_code
             left join ticket_flights tf on f.flight_id = tf.flight_id
             left join tickets t on tf.ticket_no = t.ticket_no
    group by a.aircraft_code,
             a.model,
             scheduled_departure_date
)

select aircraft_code,
       model,
       avg(unique_passenger_id_cnt) as cnt
from unique_passengers_per_date_and_aircraft
group by aircraft_code, model
order by cnt desc
limit 5;
-- Для хэш-таблицы создается 8 временных файлов, это не очень гуд, поэтому увеличиваем work_mem до 120 mB (место на диске использоваться не будет).
-- Что уменьшило время выполнения в 1.2 раза.
-- Sort Method: quicksort  Memory: 112715kB сортировка целиком проведена в оперативной памяти.
-- Так же используется quiсksort, можно попробовать заменить на merge sort. Но это ничего не даст, так как нам удобнее построить 
-- хэш-таблицу и пробежаться по ней при мердже.
-- Про создание индексов точно не могу сказать, просто я думаю, что merge join будет неэффективно использоваться.
