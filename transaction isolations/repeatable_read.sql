-- Здесь нас не интересуют операции, связанные с изменением или удалением данных, но интересуют ones,
-- связанные с добавлением новых записей. Таким образом, под этот уровень изоляции подходит транзакция,
-- связанная с получением информации по городам, в которых существуют (или когда-то существовали) жд станции
-- (т.е. по городам, которые представлены в бд в таблице stations). Потому что здесь нам интересно получение (INSERT)
-- новых станций и не интересно удаление (DELETE) или изменение (UPDATE) каких-то данных (например, названия) уже имеющихся.


-- transaction 1;
BEGIN;

-- transaction 2;
SELECT count(name) FROM stations;

-- transaction 1;
UPDATE stations SET name = ‘Weirdo2’
WHERE city = ‘Dnepropetrovsk’
INSERT INTO stations (name, city) VALUES (‘Weirdo’, ‘Ruslanovsk’);
COMMIT;

-- transaction 2;
SELECT count(name) FROM stations;
