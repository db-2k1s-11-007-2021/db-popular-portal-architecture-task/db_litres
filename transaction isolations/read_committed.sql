-- Тут нам важно иметь только закоммиченные изменения. В нашем случае это может быть регистрация пассажиров на поезд.
-- Одна транзакция меняет формат хранения ФИО в таблице Customer, при этом в ней может быть допущена ошибка, а другая в
-- это время формирует список пассажиров на поезд, надо бы чтобы все фио были в одинаковом формате.


-- transaction 1
BEGIN;
UPDATE customers SET name=’Mr’||name WHERE id=1245;
UPDATE customers SET name=’Ms’||name WHERE id=1244;

-- transaction 2
SELECT name, t.id, t.train_id FROM customers left join tickets as t on custumers.id where t.train_id = 23;

UPDATE customers SET name=’Mr’||name WHERE id=badid;
ROLLBACK WORK;
